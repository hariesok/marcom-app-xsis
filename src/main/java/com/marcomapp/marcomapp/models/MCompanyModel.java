package com.marcomapp.marcomapp.models;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PostPersist;
import javax.persistence.Table;

@Entity
@Table(name="m_company")
public class MCompanyModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, length = 11)
    private Long id;

    @Column(name="code", nullable=false, length= 50)
    private String code;
    @PostPersist
    public void generateUnitCode() {
    if(id.toString().length() == 1) code = ("CP000" + id);
    else if(id.toString().length() == 2) code = ("CP00" + id);
    else if(id.toString().length() == 3) code = ("CP0" + id);
    else if(id.toString().length() == 4) code = ("CP" + id);
    }

    @Column(name="name", length = 50, unique = true, nullable = false)
    public String name;

    @Column(name="address")
    public String address;

    @Column(name="phone", length=50)
    public String phone;

    @Column(name="email", length=50)
    public String email;
    
    @Column(name="is_delete", nullable=false, columnDefinition = "boolean default false")
    public boolean isDelete;

    @Column(name="created_by", length=50)
    public String createdBy;

    @Column(name="created_date")
    public Date createdDate;
    
    @Column(name="updated_by", length = 50)
    public String updatedBy;

    @Column (name="updated_date")
    public Date updatedDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

}