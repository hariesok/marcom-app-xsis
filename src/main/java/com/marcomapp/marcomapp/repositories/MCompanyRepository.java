package com.marcomapp.marcomapp.repositories;

import com.marcomapp.marcomapp.models.MCompanyModel;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MCompanyRepository extends JpaRepository<MCompanyModel, Long> {
    
}