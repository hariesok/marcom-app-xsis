package com.marcomapp.marcomapp.controllers;

import java.sql.Date;
import java.util.List;

import com.marcomapp.marcomapp.models.MCompanyModel;
import com.marcomapp.marcomapp.repositories.MCompanyRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("master/company")
public class MCompanyController {
    @Autowired
    private MCompanyRepository mcompanyrepo;

    @GetMapping("index")
    public ModelAndView index() {
        ModelAndView view = new ModelAndView("/mCompany/index");
        List<MCompanyModel> company = this.mcompanyrepo.findAll();
        view.addObject("company", company);
        return view;
    }

    @GetMapping("add")
    public ModelAndView add() {
        ModelAndView view = new ModelAndView("/mCompany/add");
        MCompanyModel company = new MCompanyModel();
        view.addObject("company", company);
        return view;
    }

    @GetMapping("edit/{id}")
    public ModelAndView edit(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("mCompany/add");
        MCompanyModel company = this.mcompanyrepo.findById(id).orElse(null);
        view.addObject("company", company);
        return view;
    }

    @PostMapping("save")
    public ModelAndView save(@ModelAttribute MCompanyModel company, BindingResult result) {
        if(!result.hasErrors()) {
            if(company.createdDate == null) {
                company.setCreatedBy("Admin");
                Date date = new Date(System.currentTimeMillis());
                company.setCreatedDate(date);
            } else {
                company.setUpdatedBy("Admin");
                Date date = new Date(System.currentTimeMillis());
                company.setUpdatedDate(date);
            }
            this.mcompanyrepo.save(company);
            return new ModelAndView("redirect:/master/company/index");
        } else {
            return new ModelAndView("redirect:/master/company/add");
        }
    }
    @PostMapping("delete/{id}")
    public ModelAndView delete(@PathVariable("id") Long id, @ModelAttribute MCompanyModel company) {
        if(this.mcompanyrepo.findById(id) != null) {
            company.setDelete(true);
            this.mcompanyrepo.saveAndFlush(company);
        }
        return new ModelAndView("redirect:/master/company/index");
    }

}