function findCompany() {
    var code = document.getElementById("search-code").value;
    var name = document.getElementById("search-name").value;
    var date = document.getElementById("search-date").value;
    var creator = document.getElementById("search-creator").value;

    console.log({code, name, date, creator});
}
function addCompany() {
    $.ajax({
		url:'/master/company/add',
		type:'get',
		contentType:'html',
		success:function(result) {
			$(".modal-title").html("Add Company");
			$(".modal-body").html(result);
			$("#mymodal").modal("show");
		}
	})
}

function companyDetails(data) {
    dataId = data.dataset.id;
    alert(dataId)
}
function companyEdit(data) {
    dataId = data.dataset.id;
    dataName = data.dataset.name;
    $.ajax({
        url:'/master/company/edit/'+dataId,
        type:'get',
        contentType:'html',
        success: function(result) {
            $(".modal-title").html("Edit Company " + dataName);
            $(".modal-body").html(result);
            $("#mymodal").modal("show");
        }
    })
}
function companyDelete(data) {
    dataId = data.dataset.id;
    dataName = data.dataset.name;
    $(".modal-title").html("Delete data");
    $(".modal-body").html(`
    <form action="/master/company/delete/${dataId}" method="POST">
    Delete this data? <br> Company name: ${dataName}  <hr>
    <button type="submit" class="btn btn-primary">Save</button>
    <button class="btn btn-warning" data-dismiss="modal">Cancel</button>
    `);
    $("#smallmodal").modal("show");
}
function showAlert(alertText){
    alert(alertText);
}